import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  
url : any = "https://www.metaweather.com/api/location/" 
proxy : any = "https://cors-anywhere.herokuapp.com/"


getWeather(ID){
  console.log(ID)
  return this.http.get(
    `${this.proxy}${this.url}${ID}`
  );
}


}