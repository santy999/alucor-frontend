import { Component, OnInit } from '@angular/core';
import { ServiceService } from './service.service';
import { FormsModule, FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Alucor';

  public weatherSearchForm: FormGroup;

  
  constructor( private service : ServiceService, private formBuilder: FormBuilder ){}

weatherDetails : any;

  ngOnInit(){

    this.weatherSearchForm = this.formBuilder.group({
      location: ['']
    });
  }

  ID;
  errMsg = null;
  loading = false;

  getWeatherReport(formsValue){
    try {
    this.loading = true
    console.log(formsValue,formsValue.location.length,typeof(formsValue));
    if((formsValue.location === 'london' || 'London' || 'London') && (formsValue.location.length == 6)){
      console.log("iffffffffff")
        this.ID = 44418
    }else if ((formsValue.location === 'san Francisco' || 'sanfrancisco' ||
                 'San Francisco' || 'San francisco' || 'sanFrancisco') && (formsValue.location.length == 13 ||
                  formsValue.location.length == 12)){
      console.log("inside elllll")
      this.ID = 2487956
    }else{
      this.errMsg = "The Place Your Entered Is Not Exists In Server"
    }
    this.service.getWeather(this.ID).subscribe(data => {
        this.loading = false;
        this.weatherDetails = data;
      console.log("final",this.weatherDetails);
      });
      formsValue.reset()
  }
  
catch(error) {
  }
}
}
